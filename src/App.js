import React from 'react';

import {HashRouter as Router, Route} from 'react-router-dom';

import 'semantic-ui-css/semantic.min.css';
import './App.css';

import Login from './components/login'
import Dashboard from './components/dashboard/Dashboard'


export default function App() {
	return (
		<Router>
			<Route exact path="/" component={Login} />
			<Route path="/dashboard" component={Dashboard} />
		</Router>
	);
}
