import React from 'react';

import {Button, Form} from 'semantic-ui-react';

import './login.css';


export default function Login(props) {
	return (
		<div id='login'>
			<section>
				<img
				 alt='Hyloq: Stand out in the noise'
				 src='https://i.ibb.co/y0vHfD0/LOGO-MAIN-LIGHT-HEADLINE.png'
				 width={450}
				 height={127}/>
				
				<Form action='/#/dashboard'>
					<Form.Input placeholder='Username or phone number'/>
					<Form.Input placeholder='Password' type='password'/>
					
					<nav>
						<a href='/#/dashboard'>Don&apos;t have an account?</a>
						<div>
							<a href='/#/dashboard'>Sign up</a>
							<a href='/#/dashboard'>Reset password</a>
						</div>
					</nav>
					
					<Button fluid size='medium' content='Login' primary/>
				</Form>
				
				<p>By signing in, you agree to our Terms and that you have
				read our privacy policy and Content Policy.</p>
			</section>
		</div>
	);
}
