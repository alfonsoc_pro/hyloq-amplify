import React, {Component} from 'react';

import {Container, Grid, Image} from 'semantic-ui-react';

import HeaderComponent from '../header';
import SettingsMenu from './sidebar';


export default class SettingsHome extends Component {
	constructor(props) {
		super(props);
		this.handleRenderComponent = this.handleRenderComponent.bind(this);
	} 
    
    state = {
		selection: 'home'
    };
    
	handleRenderComponent(selection, sidebar='dashboard') {
		this.setState({
			selection: selection,
			sidebar: sidebar
		});
    }
    
	render() {
		var selection = (
			<>
				<h1>
					<Image src='http://localhost/skall.png' />
					Hello, <a href='https://duckduckgo.com'>Smug Swordsman</a>!
				</h1>
				<Grid>
					<Grid.Row>
						<Grid.Column>
							Account
						</Grid.Column>
						<Grid.Column>
							Users
						</Grid.Column>
					</Grid.Row>
					<Grid.Row>
						<Grid.Column>
							Lists
						</Grid.Column>
						<Grid.Column>
							Billing
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</>);
		
		return (
			<Container fluid>
				<HeaderComponent />
				<Grid>
					<Grid.Row>
						<Grid.Column width={3}>
							<SettingsMenu
							 highlight={this.state.selection}
							 handleRenderComponent=
								{this.handleRenderComponent} />
						</Grid.Column>
						
						<Grid.Column width={11}>
							{selection}
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</Container>
		);
	}
}
