import React from 'react';

import {Menu, Icon} from 'semantic-ui-react';

import './menu.css';


export default function SettingsMenu(props) {
	const handleItemClick = (e, {name}) => {
		props.handleRenderComponent(name);  
	}
	
	return (
		<Menu secondary vertical>
			<Menu.Item name='home'
			 className='head_title'
			 active=
				{props.highlight === 'home'}
			 onClick={handleItemClick}>
				<Icon name='group' />
				Home
			</Menu.Item>
			
			<Menu.Item name='account'
			 className='head_title'
			 active=
				{props.highlight === 'account'}
			 onClick={handleItemClick}>
				<Icon name='group' />
				Account
			</Menu.Item>
			
			<Menu.Item name='lists'
			 className='head_title'
			 active=
				{props.highlight === 'lists'}
			 onClick={handleItemClick}>
				<Icon name='group' />
				Lists
			</Menu.Item>
			
			<Menu.Item name='billing'
			 className='head_title'
			 active=
				{props.highlight === 'billing'}
			 onClick={handleItemClick}>
				<Icon name='group' />
				Billing
			</Menu.Item>
			
			<Menu.Item name='users'
			 className='head_title'
			 active=
				{props.highlight === 'users'}
			 onClick={handleItemClick}>
				<Icon name='group' />
				Users
			</Menu.Item>
		</Menu>
	);
}
