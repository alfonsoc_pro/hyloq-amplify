import React, {Component} from 'react';

import {Dropdown, Image, Input} from 'semantic-ui-react';

import './header.css';


function userTrigger() {
	return (
		<div id='user'>
			<Image circular avatar
			 src='https://react.semantic-ui.com/images/avatar/small/elliot.jpg'
			 alt='' />
			Haytham
		</div>
	);
}


var opt = [
	{key: 'acct', text: 'Account', value: 'settings-account'},
	{key: 'list', text: 'Lists', value: 'settings-lists'},
	{key: 'bill', text: 'Billing', value: 'settings-billing'},
	{key: 'user', text: 'Users', value: 'settings-users'}
];


export default function HeaderComponent(props) {
	const handleChange = (e, data) => {
		if(data.value) {
			props.handleRenderComponent(data.value);
		}
	}
	
	return (
		<header id='header'>
			<h1>
				<Image
				 src='http://localhost/LOGO_MAIN_LIGHT.png'
				 alt='HyloQ'
				 width={150}
				 height={42} />
			</h1>
			<Input id='header-search' placeholder='Search…' />
			<nav>
				<Dropdown
				 trigger={userTrigger()}
				 options={opt}
				 icon='chevron down'
				 onChange={handleChange} />
			</nav>
		</header>
	);
}
