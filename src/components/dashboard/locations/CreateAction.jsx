import React, {Component} from 'react';

import {Button, Dropdown, Header, Icon, Input} from 'semantic-ui-react';

import Map from './map/Map';

import './styles/CreateAction.css';


export default class CreateAction extends Component {
	action = (category, h2, description) => (<>
		<section className='hyloq-blue'>
			<header className='compressed'>
				<h2>{h2}</h2>
				<p>{description}</p>
			</header>
			<div className='columns'>
				<div>
					<ul>
						<li>
							<select className='hyloq-darkblue'>
								<option>Select ad group</option>
							</select>
						</li>
						<li>
							<input type='radio' name={category + '-act'} id={category + '-noact'} />
							<label for={category + '-noact'}>No action</label>
						</li>
						<li>
							<input type='radio' name={category + '-act'} id={category + '-disp'} />
							<label for={category + '-disp'}>Display on entry</label>
						</li>
						<li>
							<input type='radio' name={category + '-act'} id={category + '-bid'} />
							<label for={category + '-bid'}>Change bid</label>
						</li>
					</ul>
				</div>
				<div>
					<ul>
						<li>
							<input type='radio' name={category + '-pt'} id={category + '-all'} />
							<label for={category + '-sel'}><strong>Selection:</strong> All points</label>
						</li>
						<li>
							<input type='radio' name={category + '-pt'} id={category + '-sel'} />
							<label for={category + '-sel'}><strong>Selection:</strong> Selected points</label>
						</li>
					</ul>
					<p>Aliquam eu libero mauris. Cras vitae eros nec massa auctor lacinia nec vitae lectus. Etiam sed eleifend nibh. Quisque aliquam sed libero id efficitur. Etiam eget aliquet lacus. Aliquam nisl mi, ornare sed lacinia nec, vestibulum eget justo. Nullam placerat eget massa eu dapibus. Pellentesque arcu augue, ultricies in magna id, volutpat rhoncus nisi. Nunc eleifend eget neque non porta. Praesent eleifend porta lacus, vel sollicitudin lorem tempus ac. Nam eget velit in nisl aliquet gravida. Vestibulum dictum mi consectetur aliquam egestas. In a elementum risus, quis eleifend dui. Praesent tincidunt neque eget ante facilisis tincidunt.</p>
				</div>
			</div>
			<Map />
			<footer>
				<button className='hyloq-darkblue'>Save action</button>
			</footer>
		</section>
	</>);
	
	render() {
		return (
			<>
				<Header as='h1' className='highlight'>New Action</Header>
				
				<section>
					<div className='lined'>
						<label for='action-name'>Action Name</label>
						<input id='action-name' />
					</div>
				</section>
				
				{this.action('entry', 'Enter Action', 'Display on entry')}
				{this.action('dwell', 'Dwell Action', 'Display after 15 minutes')}
				{this.action('exit', 'Exit Action', 'Display after 15 minutes')}
			</>
		);
	}
}
