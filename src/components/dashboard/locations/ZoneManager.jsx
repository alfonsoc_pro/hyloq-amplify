import React, {Component} from 'react';

import {Container, Divider, Grid, Segment} from 'semantic-ui-react';
import {Button, Checkbox, Icon, Dropdown, Input} from 'semantic-ui-react';
import {Header, List} from 'semantic-ui-react';

import Map from './map/Map';
import ZoneButton from './ZoneButton';


export default class ZoneManager extends Component {
	constructor(props) {
		super(props);
		this.stepZoneCreator = this.stepZoneCreator.bind(this);
	}
	
	state = {
		zone_creator: 0,
		zone_name: ''
	};
	
	stepZoneCreator(step, zone='') {
		if(!zone) {
			this.setState({
				zone_creator: step
			});
		}
		else {
			this.setState({
				zone_creator: step,
				zone_name: zone
			});
		}
	}
	
	zoneOptions() {
		var zones = [];
		var keys = Object.keys(localStorage);
		keys.sort();
		for(var z in keys) {
			if(keys[z][0] !== 'z') continue;
			zones.push({
				key: z,
				text: keys[z].substring(1, keys[z].length),
				value: z});
		}
		
		return (
			<>
				<Header size='huge' textAlign='center'>
					Add/Remove Locations
				</Header>
				<Segment placeholder>
					<Grid columns={2} stackable textAlign='center'>
						<Divider vertical>or</Divider>
						
						<Grid.Row verticalAlign='middle'>
							<Grid.Column>
								<Header icon>
									<Icon name='search' />
									Add Location to Zone
								</Header>
								
								<Dropdown
								 button
								 className='icon'
								 floating
								 labeled
								 icon='world'
								 options={zones}
								 onChange={(e) => {
									if(!e.target.textContent) return;
									this.stepZoneCreator(
										2, e.target.textContent);
								 }}
								 search
								 text='Select Zone' />
							</Grid.Column>
							
							<Grid.Column>
								<Header icon>
									<Icon name='world' />
									Make New Zone
								</Header>
								<Button primary
								 onClick={() => {this.stepZoneCreator(1);}}>
									Create
								</Button>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Segment>
			</>
		);
	}
	
	newZoneOptions() {
		return (
			<>
				<Header size='huge' textAlign='center'>
					Creating New Zone
				</Header>
				<Segment placeholder>
					<Grid columns={1} stackable textAlign='center'>
						<Grid.Row verticalAlign='middle'>
							<Grid.Column>
								<Input
								 placeholder='Enter a name for the zone' />
								<div>
									<Button.Group>
										<Button primary
										 onClick={() => {
											this.stepZoneCreator(2, 'New Zone');
										}}>
											OK
										</Button>
										<Button.Or />
										<Button secondary
										 onClick={() => {
											this.stepZoneCreator(0);
										}}>
											Cancel
										</Button>
									</Button.Group>
								</div>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Segment>
			</>
		);
	}
	
	addLocOptions() {
		return (
			<>
				<Header size='huge' textAlign='center'>
					Adding Locations to {this.state.zone_name}
				</Header>
				<Segment placeholder>
					<Grid columns={3} stackable textAlign='center'>
						<Grid.Row verticalAlign='middle'>
							<Grid.Column>
								<Header icon>
									<Icon name='square outline' />
									Add Polygon
								</Header>
								<Button primary
								 onClick={() => {this.stepZoneCreator(3);}}>
									Start
								</Button>
							</Grid.Column>
							
							<Grid.Column>
								<Header icon>
									<Icon name='circle outline' />
									Add Circle
								</Header>
								<Button primary
								 onClick={() => {this.stepZoneCreator(4);}}>
									Start
								</Button>
							</Grid.Column>
							
							<Grid.Column>
								<Header icon>
									<Icon name='x' />
									Go Back
								</Header>
								<Button secondary
								 onClick={() => {this.stepZoneCreator(0);}}>
									Cancel
								</Button>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Segment>
			</>
		);
	}
	
	polygonOptions() {
		return (
			<>
				<Header size='huge' textAlign='center'>
					Adding Polygon(s) to {this.state.zone_name}
				</Header>
				<Segment placeholder>
					<Grid columns={1} stackable textAlign='center'>
						<Grid.Row verticalAlign='middle'>
							<Grid.Column>
								<p>lorem ipsum dolor sit amet</p>
								<Button primary
								 onClick={() => {this.stepZoneCreator(0);}}>
									Finish
								</Button>
								<Checkbox label='Add polygons in bulk' />
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Segment>
			</>
		);
	}
	
	circleOptions() {
		return (
			<>
				<Header size='huge' textAlign='center'>
					Adding Circle(s) to {this.state.zone_name}
				</Header>
				<Segment placeholder>
					<Grid columns={1} stackable textAlign='center'>
						<Grid.Row verticalAlign='middle'>
							<Grid.Column>
								<p>lorem ipsum dolor sit amet</p>
								<Button primary
								 onClick={() => {this.stepZoneCreator(0);}}>
									Finish
								</Button>
								<Checkbox label='Add circles in bulk' />
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Segment>
			</>
		);
	}
	
	renameZoneOption() {
		return (
			<>
				<Header size='huge' textAlign='center'>
					Rename {this.state.zone_name}
				</Header>
				<Segment placeholder>
					<Grid columns={1} stackable textAlign='center'>
						<Grid.Row verticalAlign='middle'>
							<Grid.Column>
								<Input
								 placeholder={
									'Enter a new name for ' +
									this.state.zone_name
								 } />
								<div>
									<Button.Group>
										<Button primary
										 onClick={() => {
											this.stepZoneCreator(0);
										}}>
											OK
										</Button>
										<Button.Or />
										<Button secondary
										 onClick={() => {
											this.stepZoneCreator(0);
										}}>
											Cancel
										</Button>
									</Button.Group>
								</div>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Segment>
			</>
		);
	}
	
	zoneSidebar() {
		var zones = [
			<List.Item key='all'>
				<Button fluid>
					Show/Hide all zones
				</Button>
			</List.Item>
		];
		
		var keys = Object.keys(localStorage);
		keys.sort();
		for(var z in keys) {
			if(keys[z][0] !== 'z') continue;
			
			var k = keys[z].substring(1, keys[z].length);
			zones.push(
				<List.Item key={keys[z]}>
					{ZoneButton(k, this.stepZoneCreator)}
				</List.Item>
			);
		}
		return (
			<List>
				{zones}
			</List>
		);
	}
	
	render() {
		var zone_creation_step;
		switch(this.state.zone_creator) {
			default:
			case 0:
				zone_creation_step = this.zoneOptions();
				break;
			case 1:
				zone_creation_step = this.newZoneOptions();
				break;
			case 2:
				zone_creation_step = this.addLocOptions();
				break;
			case 3:
				zone_creation_step = this.polygonOptions();
				break;
			case 4:
				zone_creation_step = this.circleOptions();
				break;
			case 5:
				zone_creation_step = this.renameZoneOption();
				break;
		}
		
		return (
			<Container>
				<Grid>
					<Grid.Row>
						<Grid.Column width={10}>
							<Map />
							{zone_creation_step}
						</Grid.Column>
						
						<Grid.Column width={6}>
							{this.zoneSidebar()}
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</Container>
        );
    }
}
