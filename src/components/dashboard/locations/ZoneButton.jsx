import React from 'react';

import {Button, Icon} from 'semantic-ui-react';


export default function ZoneButton(zone, callback) {
	return (
		<Button.Group fluid>
			<Button style={{width: '70%'}}>{zone}</Button>
			<Button style={{width: '15%'}} onClick={
				() => {
					callback(5, zone);
				}
			}>
				<Icon name='edit' />
			</Button>
			<Button style={{width: '15%'}}><Icon name='trash' /></Button>
		</Button.Group>
	);
}
