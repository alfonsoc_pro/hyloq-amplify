import React, {Component} from 'react';

import {Button, Header, Icon} from 'semantic-ui-react';

import Map from './map/Map';

import './styles/CreateZone.css';


export default class CreateZone extends Component {
	render() {
		return (
			<>
				<Header as='h1' className='blue'>New Zone</Header>
				
				<section id='' class='gray'>
					<Map />
				</section>
				
				<aside>
					<Button.Group>
						<Button>Zone</Button>
						<Button><Icon name='edit' /></Button>
						<Button><Icon name='trash alternate' /></Button>
					</Button.Group>
					<Button.Group>
						<Button>Zone</Button>
						<Button><Icon name='edit' /></Button>
						<Button><Icon name='trash alternate' /></Button>
					</Button.Group>
					<Button.Group>
						<Button>Zone</Button>
						<Button><Icon name='edit' /></Button>
						<Button><Icon name='trash alternate' /></Button>
					</Button.Group>
					<Button.Group>
						<Button>Zone</Button>
						<Button><Icon name='edit' /></Button>
						<Button><Icon name='trash alternate' /></Button>
					</Button.Group>
					<Button.Group>
						<Button>Zone</Button>
						<Button><Icon name='edit' /></Button>
						<Button><Icon name='trash alternate' /></Button>
					</Button.Group>
				</aside>
			</>
		);
	}
}
