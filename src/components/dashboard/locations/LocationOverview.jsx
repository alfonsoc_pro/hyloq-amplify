//import React, {Component, Fragment} from 'react';
import React, {Component} from 'react';

//import {Breadcrumb, Container, Divider, Tab} from 'semantic-ui-react';
import {Container, Tab} from 'semantic-ui-react';
//import {Table, Button, Checkbox, Icon} from 'semantic-ui-react';
import {Table, Button, Checkbox} from 'semantic-ui-react';

//import NavigationGuide from '../NavigationGuide';

import 'chart.js';


export default class LocationOverview extends Component {
	table() {
		return (
			<>
				<Button>Add Filter</Button>
				
				<Table singleLine color="red">
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell>
								<Checkbox label={{children: 'All Zones'}}
								 slider />
							</Table.HeaderCell>
							<Table.HeaderCell>Campaign</Table.HeaderCell>
							<Table.HeaderCell>Ad Group</Table.HeaderCell>
							<Table.HeaderCell>Impr.</Table.HeaderCell>
							<Table.HeaderCell>CTR</Table.HeaderCell>
							<Table.HeaderCell>Viewable CTR</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					
					<Table.Body>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox slider />
							</Table.Cell>
							<Table.Cell>Competitors-Banks</Table.Cell>
							<Table.Cell></Table.Cell>
							<Table.Cell></Table.Cell>
							<Table.Cell></Table.Cell>
							<Table.Cell></Table.Cell>
							<Table.Cell></Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox slider />
							</Table.Cell>
							<Table.Cell>Jamie Harington</Table.Cell>
							<Table.Cell>January 11, 2014</Table.Cell>
							<Table.Cell>jamieharingonton@yahoo.com</Table.Cell>
							<Table.Cell>Yes</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox slider />
							</Table.Cell>
							<Table.Cell>Campaign</Table.Cell>
							<Table.Cell>Budget</Table.Cell>
							<Table.Cell>Status</Table.Cell>
							<Table.Cell>Impr.</Table.Cell>
							<Table.Cell>CTR</Table.Cell>
							<Table.Cell>Viewable CTR</Table.Cell>
						</Table.Row>
					</Table.Body>
					
					<Table.Footer fullWidth>
						<Table.Row>
							<Table.HeaderCell>
								Total: #???
							</Table.HeaderCell>
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
						</Table.Row>
					</Table.Footer>
				</Table>
			</>
		);
	}
	
	report() {
		return 'foo';
	}
	
	render() {
		var tables = [
			{menuItem: 'Targeted', render: () => this.table()},
			{menuItem: 'Excluded', render: () => this.table()},
			{menuItem: 'Geographic Report', render: () => this.report()}
		];
		
		return (
			<Container>
				<Tab menu={{ secondary: true }} panes={tables} />
			</Container>
        );
    }
}
