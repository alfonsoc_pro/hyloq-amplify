import React, {Component} from 'react';

import {Button, Dropdown, Header, Icon, Input} from 'semantic-ui-react';

import Map from './map/Map';

import './styles/CreateFence.css';


export default class CreateFence extends Component {
	render() {
		return (
			<>
				<Header as='h1' className='highlight'>New Geo-Fence</Header>
				
				<section className='window'>
					<header>
						<Dropdown placeholder='Select a zone' className='bold' />
						<Header as='h2' id='sample-zone'>Sample Zone 1</Header>
					</header>
					<section>
						<Dropdown
						 button
						 className='icon'
						 floating
						 labeled
						 icon='world'
						 options={[]}
						 search
						 text='Select Zone' />
					</section>
				</section>
				
				<section className='columns open'>
					<div className='centered'>
						<ul id='fence-methods'>
							<li>
								<Dropdown fluid placeholder='Upload locations' />
							</li>
							<li>
								<Input placeholder='Enter address' />
								<Button><Icon name='plus' /></Button>
							</li>
							<li><Button fluid>Freehand</Button></li>
						</ul>
					</div>
					<div className='centered'>
						<p>Aliquam eu libero mauris. Cras vitae eros nec massa auctor lacinia nec vitae lectus. Etiam sed eleifend nibh. Quisque aliquam sed libero id efficitur. Etiam eget aliquet lacus. Aliquam nisl mi, ornare sed lacinia nec, vestibulum eget justo. Nullam placerat eget massa eu dapibus. Pellentesque arcu augue, ultricies in magna id, volutpat rhoncus nisi. Nunc eleifend eget neque non porta. Praesent eleifend porta lacus, vel sollicitudin lorem tempus ac. Nam eget velit in nisl aliquet gravida. Vestibulum dictum mi consectetur aliquam egestas. In a elementum risus, quis eleifend dui. Praesent tincidunt neque eget ante facilisis tincidunt.</p>
					</div>
				</section>
				
				<section id='' className='gray'>
					<Map />
				</section>
				
				<section>
					<Dropdown fluid placeholder='Edit/Add geo-fences' />
				</section>
				
				<div className='ralign'>
					<Button primary>Save geo-fence</Button>
					<Button secondary>Cancel</Button>
				</div>
			</>
		);
	}
}
