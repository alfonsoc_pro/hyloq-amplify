import React, {Component} from 'react';


export default class ZoneOverview extends Component {
	componentDidMount() {
		new window.google.maps.Map(document.getElementById('map'), {
			center: {lat: 27.972572, lng: -82.796745},
			zoom: 10,
			zoomControl: true,
			zoomControlOptions: {
				position: window.google.maps.ControlPosition.RIGHT_CENTER
			},
			scrollwheel: false,
			streetViewControl: false,
			mapTypeControl: false,
			mapTypeId: 'roadmap'
		});
	}
	
	render() {
		return <div style={{height: 600}} className="maps" id="map"></div>;
	}
}
