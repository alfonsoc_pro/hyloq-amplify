//import React, {Component, Fragment} from 'react';
import React, {Component} from 'react';

//import {Breadcrumb, Container, Divider, Tab} from 'semantic-ui-react';
import {Container, Tab} from 'semantic-ui-react';
//import {Table, Button, Checkbox, Icon} from 'semantic-ui-react';
import {Table, Button, Checkbox, Header, Icon} from 'semantic-ui-react';

import DashboardComponent from '../DashboardComponent';


export default class ActionOverview extends DashboardComponent {
	render() {
		return (
			<>
				<Header as='h1' className='highlighted'>
					All Actions
					<Button
					 icon
					 size='small'
					 name='locations_actions_create'
					 onClick={this.handleItemClick}>
						<Icon name='plus square' />
					</Button>
				</Header>
				
				<Table className='overview'>
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell>Geo-Fence</Table.HeaderCell>
							<Table.HeaderCell>Actions</Table.HeaderCell>
							<Table.HeaderCell>Campaign</Table.HeaderCell>
							<Table.HeaderCell>Ad Group</Table.HeaderCell>
							<Table.HeaderCell>Impressions</Table.HeaderCell>
							<Table.HeaderCell>
								Viewable <abbr title='Click-Through Rate'>CTR</abbr>
							</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					
					<Table.Body>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox label='Competitors - Banks' />
							</Table.Cell>
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
						</Table.Row>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox label='Jamie Harington' />
							</Table.Cell>
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
						</Table.Row>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox label='Campaign' />
							</Table.Cell>
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
						</Table.Row>
					</Table.Body>

					<Table.Footer fullWidth>
						<Table.Row>
							<Table.HeaderCell>
								Total 21
							</Table.HeaderCell>
							<Table.HeaderCell />
							<Table.HeaderCell />
							<Table.HeaderCell />
							<Table.HeaderCell />
							<Table.HeaderCell />
						</Table.Row>
					</Table.Footer>
				</Table>
			</>
		);
	}
}
