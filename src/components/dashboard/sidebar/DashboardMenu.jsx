import React from 'react';

import {Menu, Icon} from 'semantic-ui-react';

import './sidebar.css';


export default function DashboardMenu(props) {
	const handleItemClick = (e, {name}) => {
		props.handleRenderComponent(name);  
	}
	
	return (
		<Menu vertical as='nav' id='sidebar' text size='massive'>
			<Menu.Item name='overview' header
			 active={props.highlight === 'overview'}
			 onClick={handleItemClick}>
				<Icon name='globe' />
				Overview
			</Menu.Item>
			<Menu.Item name='campaigns' header
			 active={props.highlight.substring(0, 9) === 'campaigns'}
			 onClick={handleItemClick}>
				<Icon name='sliders horizontal' />
				Campaigns
			</Menu.Item>
			<Menu.Item name='campaigns'
			 active={props.highlight === 'campaigns'}
			 onClick={handleItemClick}>
				View All
			</Menu.Item>
			<Menu.Item name='campaigns-create'
			 active={props.highlight === 'campaigns-create'}
			 onClick={handleItemClick}>
				Create New
			</Menu.Item>
			
			<Menu.Item name='locations_zones' header
			 active={props.highlight.substring(0, 9) === 'locations'}
			 onClick={handleItemClick}>
				<Icon name='map signs' />
				Locations
			</Menu.Item>
			<Menu.Item name='locations_zones'
			 active={props.highlight === 'locations_zones'}
			 onClick={handleItemClick}>
				Zones
			</Menu.Item>
			<Menu.Item name='locations_fences'
			 active={props.highlight === 'locations_fences'}
			 onClick={handleItemClick}>
				Fences
			</Menu.Item>
			<Menu.Item name='locations_actions'
			 active={props.highlight === 'locations_actions'}
			 onClick={handleItemClick}>
				Actions
			</Menu.Item>
			
			<Menu.Item name='ads' header
			 active={props.highlight.substring(0, 3) === 'ads'}
			 onClick={handleItemClick}>
				<Icon name='tablet alternate' />
				Ads
			</Menu.Item>
			<Menu.Item name='ads'
			 active={props.highlight === 'ads'}
			 onClick={handleItemClick}>
				View All
			</Menu.Item>
			<Menu.Item name='ads_groups'
			 active={props.highlight === 'ads_groups'}
			 onClick={handleItemClick}>
				Ad Groups
			</Menu.Item>
			
			<Menu.Item name='reports' header
			 active={props.highlight === 'reports'}
			 onClick={handleItemClick}>
				<Icon name='file alternate' />
				Reports<span>?</span>
			</Menu.Item>
		</Menu>
	);
}
