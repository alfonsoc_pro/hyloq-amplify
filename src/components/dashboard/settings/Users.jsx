import React from 'react';

import './styles/Users.css';


const Users = () => (<>
	<h1>Users</h1>
	
	<section className='settings-panel'>
		<table>
			<thead>
				<tr>
					<th>Username</th>
					<th>Email</th>
					<th>ID</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
				<tr>
					<td>John Smithy</td>
					<td>email@internet.com</td>
					<td>42069</td>
				</tr>
			</tbody>
		</table>
	</section>
</>);


export default Users;
