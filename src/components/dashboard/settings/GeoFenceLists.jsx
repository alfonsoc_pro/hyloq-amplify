import React from 'react';

import {Button, Checkbox, Icon, Input} from 'semantic-ui-react';

import './styles/GeoFenceLists.css';


const Lists = () => (<>
	<section>
		<table className='fence-list'>
			<thead>
				<tr>
					<th>List Name</th>
					<th>Count</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Competitors - Banks</td>
					<td>8000</td>
				</tr>
				<tr>
					<td>ACU Branch - 112</td>
					<td>#</td>
				</tr>
				<tr>
					<td>Home Depot - HELOC</td>
					<td>#</td>
				</tr>
				<tr>
					<td>Lowes - HELOC</td>
					<td>#</td>
				</tr>
				<tr>
					<td>Competitors - Banks</td>
					<td>8000</td>
				</tr>
				<tr>
					<td>ACU Branch - 112</td>
					<td>#</td>
				</tr>
				<tr>
					<td>Home Depot - HELOC</td>
					<td>#</td>
				</tr>
				<tr>
					<td>Lowes - HELOC</td>
					<td>#</td>
				</tr>
				<tr>
					<td>Competitors - Banks</td>
					<td>8000</td>
				</tr>
				<tr>
					<td>ACU Branch - 112</td>
					<td>#</td>
				</tr>
				<tr>
					<td>Home Depot - HELOC</td>
					<td>#</td>
				</tr>
				<tr>
					<td>Lowes - HELOC</td>
					<td>#</td>
				</tr>
				<tr>
					<td>Competitors - Banks</td>
					<td>8000</td>
				</tr>
				<tr>
					<td>ACU Branch - 112</td>
					<td>#</td>
				</tr>
				<tr>
					<td>Home Depot - HELOC</td>
					<td>#</td>
				</tr>
				<tr>
					<td>Lowes - HELOC</td>
					<td>#</td>
				</tr>
			</tbody>
		</table>
	</section>
</>);


const NewList = () => (<>
	<section className='add-new-list'>
		<div className='uploads'>
			<Button><Icon name='cloud upload' /> Upload from desktop</Button>
			<Button><Icon name='upload' /> Upload from FTP/SFTP</Button>
			<Button><Icon name='chain' /> Upload from URL</Button>
			<Button><Icon name='server' /> Upload from server</Button>
		</div>
		<div className='import-lists'>
			<Input className='lined' label='Import Name' />
			
			<h2>Select file to import</h2>
			
			<Button primary>Change list file to upload</Button>
			<ol>
				<li>Engie_Pricing_Matrix_04_</li>
			</ol>
			
			<h2>File Attributes</h2>
			
			<table className='overview'>
				<thead>
					<tr>
						<th>Exclu.</th>
						<th>Label</th>
						<th>Code</th>
						<th>Attribute</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><Checkbox /></td>
						<td>Category_Name</td>
						<td>Category_Label</td>
						<td>Category_Value</td>
					</tr>
					<tr>
						<td><Checkbox /></td>
						<td>Category_Name</td>
						<td>Category_Label</td>
						<td>Category_Value</td>
					</tr>
					<tr>
						<td><Checkbox /></td>
						<td>Category_Name</td>
						<td>Category_Label</td>
						<td>Category_Value</td>
					</tr>
					<tr>
						<td><Checkbox /></td>
						<td>Category_Name</td>
						<td>Category_Label</td>
						<td>Category_Value</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div>
			<Button primary>Save</Button>
			<Button secondary>Cancel</Button>
			<button>???</button>
		</div>
	</section>
</>);


const GeoFenceLists = () => (<>
	<h1>Geo-fence Lists</h1>
	<Lists />
	
	<h1>Add New List</h1>
	<NewList />
</>);


export default GeoFenceLists;
