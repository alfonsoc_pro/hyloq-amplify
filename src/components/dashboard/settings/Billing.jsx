import React from 'react';

import {Button, Header, Icon, Image} from 'semantic-ui-react';

import './styles/Billing.css';


const Balance = () => (<>
	<section id='balance'>
		<header>
			<h2 className='hyloq-red columns'>
				<span>Your Balance</span>
				<span>
					<span>$25,619<span>.98</span></span>
				</span>
			</h2>
		</header>
		
		<section>
			<p>You will be charged automatically on <strong>September 5th
			</strong>. If your balance exceeds your <strong>$32,000.00
			</strong> payment threshold before that time, you will be charged
			immediately.</p>
			<progress value='25619.98' max='30000.00'></progress>
			<p>You have used <strong>$25,619.98</strong> of your <strong>
			$32,000.00</strong> payment threshold.</p>
		</section>
		
		<aside className='hyloq-gray'>
			<div><Icon name='credit card outline' /> Automatic payments</div>
			<p><Icon name='calendar alternate' /> Your last payment was 
			on <strong>August 6th</strong> for <strong>$32,000.00</strong>.
			Allow a few days for payments to be applied.</p>
		</aside>
		
		<footer>
			<Button className='hyloq-darkblue'>Make a payment</Button>
		</footer>
	</section>
</>);


const Transactions = () => (<>
	<section className='settings-summary'>
		<div>
			<Header as='h2'>Transactions</Header>
			<div>
				<ol>
					<li className='columns'>
						<span>Aug 1 - 7, 2019</span>
						<span>$1,618.08</span>
					</li>
					<li className='columns'>
						<span>Aug 1 - 7, 2019</span>
						<span>$25,487.24</span>
					</li>
					<li className='columns'>
						<span>Aug 1 - 7, 2019</span>
						<span>$9,378.00</span>
					</li>
				</ol>
			</div>
		</div>
		<Button className='hyloq-darkblue'>
			View transactions and documents
		</Button>
	</section>
</>);


const HowYouPay = () => (<>
	<section className='settings-summary'>
		<div>
			<Header as='h2'>How You Pay</Header>
			<div>
				<figure className='payment'>
					<Image src='http://localhost/skall.png' alt=''
					 width='175' height='138' />
					<figcaption>
						<div>Checking ************9302</div>
						<div>Achieva Credit Union</div>
					</figcaption>
				</figure>
			</div>
		</div>
		<Button className='hyloq-darkblue'>Manage payment methods</Button>
	</section>
</>);


const Settings = () => (<>
	<section className='settings-summary'>
		<div>
			<Header as='h2'>Settings</Header>
			<div>
				<ul>
					<li>AdWords 594-777-0293</li>
					<li>Achieva Credit Union</li>
				</ul>
			</div>
		</div>
		<Button className='hyloq-darkblue'>Manage settings</Button>
	</section>
</>);


const Billing = () => (<>
	<Header as='h1'>Billing Summary</Header>
	
	<Balance />
	
	<div>
		<Transactions />
		<HowYouPay />
		<Settings />
	</div>
</>);


export default Billing;
