import React, {Component} from 'react';

import {Button, Header, Input, Label} from 'semantic-ui-react';

import './settings.css';


const I = (props) =>
	<Input
	 type={props.type}
	 label={<label for={props.id}>{props.label}</label>}
	 id={props.id} />;


export default class CreateZone extends Component {
	render() {
		return (
			<>
				<Header as='h1'>Account Settings</Header>
				
				<section className='settings-card'>
					<button>Edit</button>
					<Header as='h2'>HYLOQ ID: 345938Y53-20398</Header>
					<div className='columns'>
						<div>
							<I label='Username' id='s-name' />
							<I label='Address' id='s-addr' />
							<I label='Apt/Suite' id='s-apt' />
							<I label='City' id='s-city' />
							<I label='ZIP' id='s-zip' />
						</div>
						<div>
							<I label='Company' id='s-comp' />
							<I label='Email' id='s-email' />
							<I label='Password' id='s-pass1' type='password' />
							<I label='Confirm password' id='s-pass2' type='password' />
							<div></div>
							<I label='Phone' id='s-phone' />
						</div>
					</div>
					<div className='columns'>
						<div>
							<section className='img-preview'>
								<div>
								</div>
								<button>Upload company logo</button>
							</section>
						</div>
						<div className='centered row confirm'>
							<Button primary>Save</Button>
							<Button secondary>Cancel</Button>
						</div>
					</div>
				</section>
				
				<Header as='h1'>Additional Settings</Header>
				
				<section className='settings-card'>
					<button>Edit</button>
					<Header as='h2'>HYLOQ ID: 345938Y53-20398</Header>
					<div className='columns'>
						<div>
							<I label='Username' id='as-name' />
							<I label='Address' id='as-addr' />
							<I label='City' id='as-city' />
							<I label='Email' id='as-email' />
						</div>
						<div className='centered row confirm'>
							<Button primary>Save</Button>
							<Button secondary>Cancel</Button>
						</div>
					</div>
				</section>
			</>
		);
	}
}
