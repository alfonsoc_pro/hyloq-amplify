import React, {Component} from 'react';

//import {Breadcrumb, Container, Divider} from 'semantic-ui-react';
import {Container} from 'semantic-ui-react';
//import {Table, Button, Checkbox, Icon} from 'semantic-ui-react';
import {Table, Button, Checkbox, Header, Icon, Label} from 'semantic-ui-react';

import {LineChart} from 'react-chartkick';

import DashboardComponent from '../DashboardComponent';
import 'chart.js';


export default class AdsOverview extends DashboardComponent {
	render() {
		return (
			<>
				<Header as='h1'>
					All Ads
					<Button
					 icon
					 size='small'
					 name='ads_create'
					 onClick={this.handleItemClick}>
						<Icon name='plus square' />
					</Button>
					<Button
					 icon
					 size='small'
					 name='ads_copy'
					 onClick={this.handleItemClick}>
						<Icon name='copy' />
					</Button>
				</Header>
				
				<LineChart 
				 data={{
					"2014-05-13": 12,
					"2015-05-22": 44,
					"2016-05-19": 29,
					"2017-05-23": 5,
					"2018-05-20": 44,
					"2019-05-14": 3,
					"2020-05-14": 0}} />
				
				<div id='table-status'>
					<Label size='big'>Campaign Status:</Label>
					<Button.Group size='big'>
						<Button>All</Button>
						<Button>Enabled</Button>
						<Button>Add Filter</Button>
					</Button.Group>
				</div>
				
				<Table className='overview'>
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell>Campaign</Table.HeaderCell>
							<Table.HeaderCell>Budget</Table.HeaderCell>
							<Table.HeaderCell>Status</Table.HeaderCell>
							<Table.HeaderCell>Impressions</Table.HeaderCell>
							<Table.HeaderCell>
								<abbr title='Click-Through Rate'>CTR</abbr>
							</Table.HeaderCell>
							<Table.HeaderCell>
								Viewable <abbr title='Click-Through Rate'>CTR</abbr>
							</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					
					<Table.Body>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox label='Competitors - Banks' />
							</Table.Cell>
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
						</Table.Row>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox label='Jamie Harington' />
							</Table.Cell>
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
						</Table.Row>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox label='Campaign' />
							</Table.Cell>
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
						</Table.Row>
					</Table.Body>

					<Table.Footer fullWidth>
						<Table.Row>
							<Table.HeaderCell>
								Total 21
							</Table.HeaderCell>
							<Table.HeaderCell />
							<Table.HeaderCell />
							<Table.HeaderCell />
							<Table.HeaderCell />
							<Table.HeaderCell />
						</Table.Row>
					</Table.Footer>
				</Table>
			</>
        );
    }
}
