import React, {Component} from 'react';

import {Table, Button, Checkbox, Dropdown, Header, Icon, Input, Label, Radio} from 'semantic-ui-react';

import './ad-duplication-form.css';


export default class CreateAd extends Component {
	render() {
		return (
			<>
				<Header as='h1'>Copy Existing Ad</Header>
				
				<section class='gray'>
					<Dropdown placeholder='Select ad group' />
					<Input placeholder='Search by name or campaign ID' />
				</section>
				
				<div class='columns'>
					<section>
						<header>
							<Header as='h2'># Campaign(s)</Header>
						</header>
						<section>
							<ol>
								<li>NASCAR</li>
								<li>ZZ Top</li>
								<li>Prcoter & Gamble</li>
							</ol>
						</section>
					</section>
					
					<section>
						<header>
							<Header as='h2'>Selected Ad Groups</Header>
						</header>
						<section>
							<ol>
								<li>NASCAR</li>
								<li>ZZ Top</li>
								<li>Prcoter & Gamble</li>
							</ol>
						</section>
					</section>
				</div>
				
				<div id='copy-ads' class='ralign'>
					<Button primary size='big'>Copy ad(s)</Button>
					<Button secondary size='big'>Cancel</Button>
				</div>
			</>
		);
	}
}
