import React from 'react';
import {Button} from 'semantic-ui-react';


export default function AdViewActions() {
	return (
		<Button.Group>
			<Button>New Ads View</Button>
			<Button>Upload Ads View</Button>
			<Button>Copy Existing Ads View</Button>
		</Button.Group>
	);
}
