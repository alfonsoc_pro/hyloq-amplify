import React, {Component} from 'react';

import {Table, Button, Checkbox, Dropdown, Header, Icon, Input, Label, Radio} from 'semantic-ui-react';

import './ad-creation-form.css';


export default class CreateAd extends Component {
	render() {
		return (
			<>
				<Header as='h1'>New Ad</Header>
				
				<section class='gray'>
					<Dropdown placeholder='Select a campaign' />
					<Input placeholder='http(s)://example.com'
					 label='Final URL:' />
				</section>
				
				<div class='columns'>
					<div>
						<section>
							<header>
								<Header as='h2'>Images and Logos</Header>
							</header>
							<section>
								<Button fluid>
									<Icon name='plus' />
									Add image
								</Button>
								<ol>
									<li>image1</li>
									<li>image2</li>
									<li>etc</li>
								</ol>
							</section>
							<aside>
								<p>At least one landscape image and one square image must be added.</p>
							</aside>
						</section>
						
						<section>
							<header>
								<Header as='h2'>Headlines/Descriptions</Header>
							</header>
							
							<section>
								<div>
									<Input className='lined' placeholder='Headlines (up to 5)' />
									<Button><Icon name='plus square' /></Button>
								</div>
								<ol class='buttons'>
									<li>
										<span>Class aptent taciti sociosqu ad litora torquent per conubia nostra</span>
										<Button><Icon name='trash alternate' /></Button>
									</li>
									<li>
										<span>Proin vehicula diam vel felis faucibus, eget faucibus neque porta</span>
										<Button><Icon name='trash alternate' /></Button>
									</li>
									<li>
										<span>Pellentesque habitant tristique senectus et netus et malesuada fames ac turpis egestas</span>
										<Button><Icon name='trash alternate' /></Button>
									</li>
									<li>
										<span>Donec et nunc at urna sodales luctus in nec metus</span>
										<Button><Icon name='trash alternate' /></Button>
									</li>
									<li>
										<span>Etiam interdum sem ac elit porttitor vestibulum</span>
										<Button><Icon name='trash alternate' /></Button>
									</li>
								</ol>
							</section>
							
							<section>
								<Input className='lined' placeholder='Long headline' />
							</section>
							
							<section>
								<div>
									<Input className='lined' placeholder='Descriptions (up to 5)' />
									<Button><Icon name='plus square' /></Button>
								</div>
								<ol class='buttons'>
									<li>
										<span>Sed orci lectus, dapibus sed consectetur a, tristique in turpis</span>
										<Button><Icon name='trash alternate' /></Button>
									</li>
									<li>
										<span>Aenean eget odio sed ligula tincidunt lobortis</span>
										<Button><Icon name='trash alternate' /></Button>
									</li>
									<li>
										<span>Morbi dui lacus, condimentum ac lobortis nec, rhoncus eget turpis</span>
										<Button><Icon name='trash alternate' /></Button>
									</li>
									<li>
										<span>Praesent tempor ex id diam accumsan, et tempor est fringilla</span>
										<Button><Icon name='trash alternate' /></Button>
									</li>
									<li>
										<span>Donec tristique tortor et nisl aliquet egestas</span>
										<Button><Icon name='trash alternate' /></Button>
									</li>
								</ol>
							</section>
							
							<section>
								<Input className='lined' placeholder='Business name' />
							</section>
						</section>
					</div>
					
					<section>
						<Button.Group as='header'>
							<Button>
								<Icon name='mobile alternate' />
								Mobile
							</Button>
							<Button>
								<Icon name='desktop' />
								Desktop
							</Button>
						</Button.Group>
						<section></section>
						<aside>
							<p>Your ads might not always include all your text and images.<br />
							Some cropping or shortening may occur in some formats, and either of your custom colors may be used.</p>
						</aside>
					</section>
				</div>
				
				<div id='create-campaign' class='ralign'>
					<Button primary size='big'>Create ad</Button>
					<Button secondary size='big'>Cancel</Button>
				</div>
			</>
		);
	}
}
