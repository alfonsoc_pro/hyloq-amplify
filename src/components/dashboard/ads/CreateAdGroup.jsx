import React, {Component} from 'react';

import {Table, Button, Checkbox, Dropdown, Header, Icon, Input, Label, Radio} from 'semantic-ui-react';

import './styles/CreateAdGroup.css';

import _ from 'lodash'

const getOptions = () =>
  _.times(3, () => {
    const name = 'faker'
    return { key: name, text: name, value: _.snakeCase(name) }
  })


export default class CreateAdGroup extends Component {
	state = {
		isFetching: false,
		multiple: true,
		search: true,
		searchQuery: null,
		value: [],
		options: getOptions(),
	}

	handleChange = (e, { value }) => this.setState({ value })
	handleSearchChange = (e, { searchQuery }) => this.setState({ searchQuery })

	fetchOptions = () => {
	this.setState({ isFetching: true })

	setTimeout(() => {
		this.setState({ isFetching: false, options: getOptions() })
		this.selectRandom()
		}, 500)
	}

	selectRandom = () => {
		const { multiple, options } = this.state
		const value = _.sample(options).value
		this.setState({ value: multiple ? [value] : value })
	}

	toggleSearch = (e) => this.setState({ search: e.target.checked })

	toggleMultiple = (e) => {
		const { value } = this.state
		const multiple = e.target.checked
		// convert the value to/from an array
		const newValue = multiple ? _.compact([value]) : _.head(value) || ''
		this.setState({ multiple, value: newValue })
	}
	
	render() {
		return (
			<>
				<Header as='h1'>New Ad Group</Header>
				
				<section id='ad-group-campaign' className='hyloq-blue block block'>
					<Dropdown placeholder='Select a campaign' />
					<Input placeholder='Ad Group Name:' />
				</section>
				
				<section id='ad-group-audience' className='hyloq-blue block'>
					<header className='compressed'>
						<Header as='h2'>Audiences</Header>
						<p>Select audiences to define who should see your
						ads</p>
					</header>
					<Dropdown
						fluid
						selection
						multiple={this.multiple}
						search={this.search}
						options={this.options}
						value={this.value}
						placeholder='Add Users'
						onChange={this.handleChange}
						onSearchChange={this.handleSearchChange}
						disabled={this.isFetching}
						loading={this.isFetching}
					/>
				</section>
				
				<section id='ad-group-demographics' className='hyloq-blue block'>
					<header className='compressed'>
						<Header as='h2'>Demographics</Header>
						<p>Reach people based on age, gender, parental status, and household income</p>
					</header>
					<Table>
						<Table.Header>
							<Table.Row>
								<Table.HeaderCell>Gender</Table.HeaderCell>
								<Table.HeaderCell>Age</Table.HeaderCell>
								<Table.HeaderCell>
									Parental Status
								</Table.HeaderCell>
								<Table.HeaderCell>
									Household Income
								</Table.HeaderCell>
							</Table.Row>
						</Table.Header>
						
						<Table.Body>
							<Table.Row>
								<Table.Cell>
									<Checkbox id='demo-gender-m'
									 label='Male' />
									<Checkbox id='demo-gender-f'
									 label='Female' />
									<Checkbox id='demo-gender-u'
									 label='Unknown' />
								</Table.Cell>
								<Table.Cell>
									<Radio id='demo-age-1824'
									 label='18 - 24' />
									<Radio id='demo-age-2534'
									 label='25 - 34' />
									<Radio id='demo-age-3544'
									 label='35 -44' />
									<Radio id='demo-age-4554'
									 label='45 - 54' />
									<Radio id='demo-age-5564'
									 label='18 - 24' />
									<Radio id='demo-age-65'
									 label='55 - 64' />
									<Radio id='demo-age-u'
									 label='Unknown' />
								</Table.Cell>
								<Table.Cell>
									<Radio id='demo-parent-n'
									 label='Not a parent' />
									<Radio id='demo-parent-y'
									 label='Parent' />
									<Radio id='demo-parent-u'
									 label='Unknown' />
								</Table.Cell>
								<Table.Cell>
									<Radio id='demo-income-10'
									 label='Top 10%' />
									<Radio id='demo-income-1120'
									 label='11 - 20%' />
									<Radio id='demo-income-2130'
									 label='21 - 30%' />
									<Radio id='demo-income-3140'
									 label='31 - 40%' />
									<Radio id='demo-income-50'
									 label='Lower 50%' />
									<Radio id='demo-income-u'
									 label='Unknown' />
								</Table.Cell>
							</Table.Row>
						</Table.Body>
					</Table>
				</section>
				
				<section id='ad-group-bid' className='hyloq-blue block'>
					<header className='compressed'>
						<Header as='h2'>Audiences</Header>
						<p>Enter your <abbr title='Cost Per Thousand ("mils" = "thousands")'>CPM</abbr> bid for this ad group</p>
					</header>
					<div className='columns'>
						<section className='centered'>
							<Input type='number' placeholder='Enter CPM Bid' />
						</section>
						<aside>
							<p>Viewable Impressions are when a user sees 50% of
							your ad or more for at least 1 second.</p>
							<p>Set the amount you want to bid per 1000 impressions. <strong>Minimum
							bid is X per 1000 impressions.</strong></p>
						</aside>
					</div>
				</section>
				
				<section id='ad-group-create' className='hyloq-blue block'>
					<header className='compressed'>
						<Header as='h2'>Create your ads</Header>
					</header>
					<div className='columns'>
						<section className='centered'>
							<Dropdown placeholder='Create new ad' />
						</section>
						<aside>
							<p>Create an ad now, or skip this step and create one
							later. <strong>Your campaign will not run without at
							least 1 ad.</strong></p>
						</aside>
					</div>
				</section>
				
				<div id='create-ad-group' class='ralign'>
					<Button primary size='big'>Create ad group</Button>
					<Button secondary size='big'>Cancel</Button>
				</div>
			</>
		);
	}
}
