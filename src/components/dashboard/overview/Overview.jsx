import React, {Component} from 'react';

import {Button, Header, Input, Table} from 'semantic-ui-react';

import ChartComponent from './chart'

import './overview.css';


export default class Overview extends Component {
	render() {
		return (
			<>
				<Header as='h1'>Overview</Header>
				
				<ChartComponent />
				
				<Header as='h1'>Reports</Header>

				<section id='reports' className='hyloq-white'>
					<Input size='mini' type="date" />
					<Input size='mini' type="date" />
					<Button.Group inline size='small'>
						<Button basic color='blue'>Search</Button>
						<Button.Or />
						<Button basic color='blue'>Export</Button>
					</Button.Group>
					
					<Table basic='very' className='overview'>
						<Table.Header>
							<Table.Row>
								<Table.HeaderCell>Name</Table.HeaderCell>
								<Table.HeaderCell>Status</Table.HeaderCell>
								<Table.HeaderCell>Notes</Table.HeaderCell>
							</Table.Row>
						</Table.Header>
						
						<Table.Body>
							<Table.Row>
								<Table.Cell>John</Table.Cell>
								<Table.Cell>Approved</Table.Cell>
								<Table.Cell>None</Table.Cell>
							</Table.Row>
							<Table.Row>
								<Table.Cell>Jamie</Table.Cell>
								<Table.Cell>Approved</Table.Cell>
								<Table.Cell>Requires call</Table.Cell>
							</Table.Row>
							<Table.Row>
								<Table.Cell>Jill</Table.Cell>
								<Table.Cell>Denied</Table.Cell>
								<Table.Cell>None</Table.Cell>
							</Table.Row>
						</Table.Body>
					</Table>
				</section>
			</>
		);
	}
}
