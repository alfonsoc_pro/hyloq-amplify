import React, {Component} from 'react';

import HeaderComponent from '../header';
import DashboardMenu from './sidebar/DashboardMenu';

import Overview from './overview/Overview';

import CampaignOverview from './campaigns/CampaignOverview';
import CreateCampaign from './campaigns/CreateCampaign';

import LocationOverview from './locations/LocationOverview';
import ZoneOverview from './locations/ZoneOverview';
import FenceOverview from './locations/FenceOverview';
import ActionOverview from './locations/ActionOverview';
import CreateZone from './locations/CreateZone';
import CreateFence from './locations/CreateFence';
import CreateAction from './locations/CreateAction';

import ZoneManager from './locations/ZoneManager';

import AdsOverview from './ads/AdsOverview';
import AdGroupsOverview from './ads/AdGroupsOverview';
import CreateAd from './ads/CreateAd';
import CopyAd from './ads/CopyAd';
import CreateAdGroup from './ads/CreateAdGroup';

import AccountSettings from './settings/AccountSettings';
import Billing from './settings/Billing';
import GeoFenceLists from './settings/GeoFenceLists';
import Users from './settings/Users';

import './dashboard.css';


export default class Dashboard extends Component {
	constructor(props) {
		super(props);
		this.handleRenderComponent = this.handleRenderComponent.bind(this);
	}
    
    state = {
		selection: 'overview'
    };
    
	handleRenderComponent(selection) {
		this.setState({
			selection: selection
		});
    }
	
    render() {
		var selection;
		switch(this.state.selection) {
			case 'overview':
			case '':
				selection = <Overview />;
				break;
			case 'campaigns':
				selection = <CampaignOverview
					shortcut={this.handleRenderComponent} />;
				break;
			case 'campaigns-create':
				selection = <CreateCampaign />;
				break;
			
			case 'locations_zones':
				selection = <ZoneOverview
					shortcut={this.handleRenderComponent} />;
				break;
			case 'locations_zones_create':
				selection = <ZoneManager />;
				break;
			case 'locations_fences':
				selection = <FenceOverview
					shortcut={this.handleRenderComponent} />;
				break;
			case 'locations_fences_create':
				selection = <CreateFence />;
				break;
			case 'locations_actions':
				selection = <ActionOverview
					shortcut={this.handleRenderComponent} />;
				break;
			case 'locations_actions_create':
				selection = <CreateAction />;
				break;
			
			case 'ads':
				selection = <AdsOverview
					shortcut={this.handleRenderComponent} />;
				break;
			case 'ads_create':
				selection = <CreateAd />;
				break;
			case 'ads_copy':
				selection = <CopyAd />;
				break;
			case 'ads_groups':
				selection = <AdGroupsOverview
					shortcut={this.handleRenderComponent} />;
				break;
			case 'ads_groups_create':
				selection = <CreateAdGroup />;
				break;
			
			case 'settings-account':
				selection = <AccountSettings />;
				break;
			case 'settings-billing':
				selection = <Billing />;
				break;
			case 'settings-lists':
				selection = <GeoFenceLists />;
				break;
			case 'settings-users':
				selection = <Users />;
				break;
			
			default:
				selection = this.state.selection;
				break;
		}
		
        return (
			<>
				<HeaderComponent
				 handleRenderComponent={this.handleRenderComponent} />
				
				<DashboardMenu
				 highlight={this.state.selection}
				 handleRenderComponent={this.handleRenderComponent} />
				
				<section id='body' className={this.state.selection}>
					{selection}
				</section>
			</>
		);
	}
}
