import React, {Component} from 'react';

import {Header, Button, Checkbox, Icon, Label, Table} from 'semantic-ui-react';

import DashboardComponent from '../DashboardComponent';

import CampaignChart from './chart/CampaignChart.jsx';


export default class CampaignOverview extends DashboardComponent {
	render() {
		return (
			<>
				<Header as='h1' size='huge'>
					All Campaigns
					<Button
					 icon
					 size='small'
					 name='campaigns_create'
					 onClick={this.handleItemClick}>
						<Icon name='plus square' />
					</Button>
				</Header>
				
				<div id='chart'>
					<CampaignChart />
				</div>
				
				<div id='table-status'>
					<Label size='big'>Campaign Status:</Label>
					<Button.Group size='big'>
						<Button>All</Button>
						<Button>Enabled</Button>
						<Button>Add Filter</Button>
					</Button.Group>
				</div>
				
				<Table className='overview'>
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell>Campaign</Table.HeaderCell>
							<Table.HeaderCell>Budget</Table.HeaderCell>
							<Table.HeaderCell>Status</Table.HeaderCell>
							<Table.HeaderCell>Impressions</Table.HeaderCell>
							<Table.HeaderCell>
								<abbr title='Click-Through Rate'>CTR</abbr>
							</Table.HeaderCell>
							<Table.HeaderCell>
								Viewable <abbr title='Click-Through Rate'>CTR</abbr>
							</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					
					<Table.Body>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox label='Competitors - Banks' />
							</Table.Cell>
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
						</Table.Row>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox label='Jamie Harington' />
							</Table.Cell>
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
						</Table.Row>
						<Table.Row>
							<Table.Cell collapsing>
								<Checkbox label='Campaign' />
							</Table.Cell>
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
							<Table.Cell />
						</Table.Row>
					</Table.Body>

					<Table.Footer fullWidth>
						<Table.Row>
							<Table.HeaderCell>
								Total 21
							</Table.HeaderCell>
							<Table.HeaderCell />
							<Table.HeaderCell />
							<Table.HeaderCell />
							<Table.HeaderCell />
							<Table.HeaderCell />
						</Table.Row>
					</Table.Footer>
				</Table>
			</>
        );
    }
}
