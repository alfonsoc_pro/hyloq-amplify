import React from 'react';
import {ResponsiveLine} from '@nivo/line'


const ChartComponent = ({ data }) => (
    <ResponsiveLine
        data={[
            {
              "id": "japan",
              "color": "hsl(300, 70%, 50%)",
              "data": [
                {
                  "x": "plane",
                  "y": 253
                },
                {
                  "x": "helicopter",
                  "y": 257
                },
                {
                  "x": "boat",
                  "y": 2
                },
                {
                  "x": "train",
                  "y": 189
                },
                {
                  "x": "subway",
                  "y": 119
                },
                {
                  "x": "bus",
                  "y": 113
                },
                {
                  "x": "car",
                  "y": 105
                },
                {
                  "x": "moto",
                  "y": 32
                },
                {
                  "x": "bicycle",
                  "y": 168
                },
                {
                  "x": "horse",
                  "y": 158
                },
                {
                  "x": "skateboard",
                  "y": 230
                },
                {
                  "x": "others",
                  "y": 85
                }
              ]
            },
            {
              "id": "france",
              "color": "hsl(300, 70%, 50%)",
              "data": [
                {
                  "x": "plane",
                  "y": 270
                },
                {
                  "x": "helicopter",
                  "y": 294
                },
                {
                  "x": "boat",
                  "y": 210
                },
                {
                  "x": "train",
                  "y": 167
                },
                {
                  "x": "subway",
                  "y": 224
                },
                {
                  "x": "bus",
                  "y": 148
                },
                {
                  "x": "car",
                  "y": 179
                },
                {
                  "x": "moto",
                  "y": 150
                },
                {
                  "x": "bicycle",
                  "y": 230
                },
                {
                  "x": "horse",
                  "y": 204
                },
                {
                  "x": "skateboard",
                  "y": 75
                },
                {
                  "x": "others",
                  "y": 113
                }
              ]
            },
            {
              "id": "us",
              "color": "hsl(182, 70%, 50%)",
              "data": [
                {
                  "x": "plane",
                  "y": 286
                },
                {
                  "x": "helicopter",
                  "y": 88
                },
                {
                  "x": "boat",
                  "y": 262
                },
                {
                  "x": "train",
                  "y": 207
                },
                {
                  "x": "subway",
                  "y": 260
                },
                {
                  "x": "bus",
                  "y": 56
                },
                {
                  "x": "car",
                  "y": 95
                },
                {
                  "x": "moto",
                  "y": 155
                },
                {
                  "x": "bicycle",
                  "y": 264
                },
                {
                  "x": "horse",
                  "y": 135
                },
                {
                  "x": "skateboard",
                  "y": 11
                },
                {
                  "x": "others",
                  "y": 161
                }
              ]
            },
            {
              "id": "germany",
              "color": "hsl(267, 70%, 50%)",
              "data": [
                {
                  "x": "plane",
                  "y": 296
                },
                {
                  "x": "helicopter",
                  "y": 270
                },
                {
                  "x": "boat",
                  "y": 116
                },
                {
                  "x": "train",
                  "y": 63
                },
                {
                  "x": "subway",
                  "y": 242
                },
                {
                  "x": "bus",
                  "y": 200
                },
                {
                  "x": "car",
                  "y": 202
                },
                {
                  "x": "moto",
                  "y": 221
                },
                {
                  "x": "bicycle",
                  "y": 6
                },
                {
                  "x": "horse",
                  "y": 22
                },
                {
                  "x": "skateboard",
                  "y": 244
                },
                {
                  "x": "others",
                  "y": 85
                }
              ]
            },
            {
              "id": "norway",
              "color": "hsl(356, 70%, 50%)",
              "data": [
                {
                  "x": "plane",
                  "y": 4
                },
                {
                  "x": "helicopter",
                  "y": 25
                },
                {
                  "x": "boat",
                  "y": 173
                },
                {
                  "x": "train",
                  "y": 206
                },
                {
                  "x": "subway",
                  "y": 211
                },
                {
                  "x": "bus",
                  "y": 113
                },
                {
                  "x": "car",
                  "y": 248
                },
                {
                  "x": "moto",
                  "y": 277
                },
                {
                  "x": "bicycle",
                  "y": 230
                },
                {
                  "x": "horse",
                  "y": 110
                },
                {
                  "x": "skateboard",
                  "y": 42
                },
                {
                  "x": "others",
                  "y": 119
                }
              ]
            }
          ]
        }
        margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
        xScale={{ type: 'point' }}
        yScale={{ type: 'linear', stacked: true, min: 'auto', max: 'auto' }}
        axisTop={null}
        axisRight={null}
        axisBottom={{
            orient: 'bottom',
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: 'transportation',
            legendOffset: 36,
            legendPosition: 'middle'
        }}
        axisLeft={{
            orient: 'left',
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: 'count',
            legendOffset: -40,
            legendPosition: 'middle'
        }}
        colors={{ scheme: 'nivo' }}
        pointSize={10}
        pointColor={{ theme: 'background' }}
        pointBorderWidth={2}
        pointBorderColor={{ from: 'serieColor' }}
        pointLabel="y"
        pointLabelYOffset={-12}
        useMesh={true}
        legends={[
            {
                anchor: 'bottom-right',
                direction: 'column',
                justify: false,
                translateX: 100,
                translateY: 0,
                itemsSpacing: 0,
                itemDirection: 'left-to-right',
                itemWidth: 80,
                itemHeight: 20,
                itemOpacity: 0.75,
                symbolSize: 12,
                symbolShape: 'circle',
                symbolBorderColor: 'rgba(0, 0, 0, .5)',
                effects: [
                    {
                        on: 'hover',
                        style: {
                            itemBackground: 'rgba(0, 0, 0, .03)',
                            itemOpacity: 1
                        }
                    }
                ]
            }
        ]}
    />
);


export default ChartComponent;
