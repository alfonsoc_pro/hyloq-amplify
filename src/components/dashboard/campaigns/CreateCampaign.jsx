import React, {Component} from 'react';

import {Header, Button, Divider, Grid} from 'semantic-ui-react';
import {Form, Input, Icon, Label, Dropdown, Checkbox, Radio} from 'semantic-ui-react';

import './styles/CreateCampaign.css';


let map;
let bounds = new window.google.maps.LatLngBounds();
let sub_area;
let coordinates = [];
let color = [
	'#FF0000',
	'#4286f4',
	'#ffff00',
	'#ff00b2',
	'#bb00ff',
	'#00ffff',
	'#26ff00',
	'#00ff87'
];

const optionsLang = [
  {key: 'English', text: 'English', value: 'English'},
  {key: 'French', text: 'French', value: 'French'},
  {key: 'Spanish', text: 'Spanish', value: 'Spanish'},
  {key: 'German', text: 'German', value: 'German'},
  {key: 'Chinese', text: 'Chinese', value: 'Chinese'},
  { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  { key: 'ax', value: 'ax', flag: 'ax', text: 'Aland Islands' },
  { key: 'al', value: 'al', flag: 'al', text: 'Albania' },
  { key: 'dz', value: 'dz', flag: 'dz', text: 'Algeria' },
  { key: 'as', value: 'as', flag: 'as', text: 'American Samoa' },
  { key: 'ad', value: 'ad', flag: 'ad', text: 'Andorra' },
  { key: 'ao', value: 'ao', flag: 'ao', text: 'Angola' },
  { key: 'ai', value: 'ai', flag: 'ai', text: 'Anguilla' },
  { key: 'ag', value: 'ag', flag: 'ag', text: 'Antigua' },
  { key: 'ar', value: 'ar', flag: 'ar', text: 'Argentina' },
  { key: 'am', value: 'am', flag: 'am', text: 'Armenia' },
  { key: 'aw', value: 'aw', flag: 'aw', text: 'Aruba' },
  { key: 'au', value: 'au', flag: 'au', text: 'Australia' },
  { key: 'at', value: 'at', flag: 'at', text: 'Austria' },
  { key: 'az', value: 'az', flag: 'az', text: 'Azerbaijan' },
  { key: 'bs', value: 'bs', flag: 'bs', text: 'Bahamas' },
  { key: 'bh', value: 'bh', flag: 'bh', text: 'Bahrain' },
  { key: 'bd', value: 'bd', flag: 'bd', text: 'Bangladesh' },
  { key: 'bb', value: 'bb', flag: 'bb', text: 'Barbados' },
  { key: 'by', value: 'by', flag: 'by', text: 'Belarus' },
  { key: 'be', value: 'be', flag: 'be', text: 'Belgium' },
  { key: 'bz', value: 'bz', flag: 'bz', text: 'Belize' },
  { key: 'bj', value: 'bj', flag: 'bj', text: 'Benin' }
];

const devices =[
	{key: 'all', text: 'All devices', value: 'all'},
	{key: 'mobile', text: 'Mobile', value: 'mobile'},
]

const days = [
	{key: 'Sunday', text: 'Sunday', value: 'sun'},
	{key: 'Monday', text: 'Monday', value: 'mon'},
	{key: 'Tuesday', text: 'Tuesday', value: 'tue'},
	{key: 'Wednesday', text: 'Wednesday', value: 'wed'},
	{key: 'Thursday', text: 'Thursday', value: 'thu'},
	{key: 'Friday', text: 'Friday', value: 'fri'},
	{key: 'Saturday', text: 'Saturday', value: 'sat'}
];

const months = [
	{key: 'January', text: 'January', value: 'January'},
	{key: 'February', text: 'February', value: 'February'},
	{key: 'March', text: 'March', value: 'March'},
	{key: 'April', text: 'April', value: 'April'},
	{key: 'May', text: 'May', value: 'May'},
	{key: 'June', text: 'June', value: 'June'},
	{key: 'July', text: 'July', value: 'July'},
	{key: 'August', text: 'August', value: 'August'},
	{key: 'September', text: 'September', value: 'September'},
	{key: 'October', text: 'October', value: 'October'},
	{key: 'November', text: 'November', value: 'November'},
	{key: 'December', text: 'December', value: 'December'}
];

const scale = [
	{key: 'campaign', text: 'The whole campaign', value: 'campaign'}
];

const period = [
	{key: 'Month', text: 'Month', value: 'Month'},
	{key: 'Quarter', text: 'Quarter', value: 'Quarter'},
	{key: 'Year', text: 'Year', value: 'Year'}
];

export default class CreateCampaign extends Component {
	render() {
		var optimize_label1 =
			(<label>Optimize: <span>Prefer best performing ads</span></label>);
		var optimize_label2 =
			(<label>Do not optimize: <span>Rotate ads indefinitely</span></label>);
		
		return (
			<>
				<Header as='h1' size='huge'>
					Create Campaign
				</Header>
				
				<section className='hyloq-blue block'>
					<label for='campaign-name' className='lined'>
						Campaign Name
					</label>
					<input id='campaign-name' type='text' className='lined' />
				</section>
				
				<section className='hyloq-blue block'>
					<header className='compressed'>
						<Header as='h2'>Languages</Header>
						<p>Select the language(s) your customers speak.</p>
					</header>
					<div className='columns'>
						<section>
							<Dropdown
							 options={optionsLang}
							 multiple search selection
							 fluid
							 placeholder='Select one or more languages' />
						</section>
						<aside>
							<p>To show ads to people with Spanish as a language
							preference, select Spanish as your campaign language
							and use Spanish ad text and keywords.</p>
						</aside>
					</div>
				</section>
				
				<section className='hyloq-blue block'>
					<header className='compressed'>
						<Header as='h2'>Bidding</Header>
						<p>Viewable impressions</p>
					</header>
					<div className='columns'>
						<section>
							<Input placeholder='Enter per-CPM bid'
							 className='lined' />
						</section>
						<aside>
							<p>&ldquo;Viewable impressions&rdquo; are when a
							user sees 50% or more of your ad for at least one
							second.</p>
							<p>Set the amount you want to bid per 1000
							impressions. Minimum bid is <strong>X per 1000
							impressions</strong>.</p>
						</aside>
					</div>
				</section>
				
				<section className='hyloq-blue block'>
					<header className='compressed'>
						<Header as='h2'>Budget</Header>
						<p>Enter the average amount you want to spend each day</p>
					</header>
					<div className='columns'>
						<section>
							<Input placeholder='$' className='lined' />
						</section>
						<aside>
							<p>For the month, you won&apos;t pay more than your
							daily budget times the average number of days in a
							month. Some days you might spend more or less than
							your daily budget.</p>
						</aside>
					</div>
				</section>
				
				<section className='hyloq-blue block'>
					<header className='compressed'>
						<Header as='h2'>Ad Schedule</Header>
					</header>
					<div className='columns'>
						<section>
							<Radio name='optimize' id='opt-y'
							 label={optimize_label1} />
							<Radio name='optimize' id='opt-n'
							 label={optimize_label2} />
						</section>
						<aside>
							<p>This copy will change depending on which &apos;Ad
							Rotation&apos; selection is made.</p>
						</aside>
					</div>
				</section>
				
				<section id='ad-rotation' className='hyloq-blue block'>
					<header className='compressed'>
						<Header as='h2'>Ad Rotation</Header>
					</header>
					<div className='columns'>
						<section>
							<ul>
								<li>
									<select className='hyloq'>
										<option>Sunday</option>
										<option>Monday</option>
										<option>Tuesday</option>
										<option>Wednesday</option>
										<option>Thursday</option>
										<option>Friday</option>
										<option>Saturday</option>
									</select>
									from
									<input type='time' />
									to
									<input type='time' />
								</li>
							</ul>
							<Button fluid>
								<Icon name='plus' />
								Add
							</Button>
							<p>Based on account time zone: <strong>TIMEZONE</strong>.
							Saving this removes the settings you changed and
							adds new ones, resetting any performance data.</p>
						</section>
						<aside>
							<p>To limit when your ads can run, set an ad 
							schedule. Keep in mind that your ads will only run
							during these times.</p>
						</aside>
					</div>
				</section>
				
				<section className='hyloq-blue block'>
					<header className='compressed'>
						<Header as='h2'>Devices</Header>
					</header>
					<div className='columns'>
						<section>
							<Dropdown
							 options={devices}
							 defaultValue={['all']}
							 multiple search selection
							 fluid
							 placeholder='Select which devices will show your ads' />
						</section>
						<aside>
							<p>Device targeting lets you choose the types of
							devices where your ad can appear.</p>
						</aside>
					</div>
				</section>
				
				<section className='hyloq-blue block'>
					<header className='compressed'>
						<Header as='h2'>Frequency</Header>
						<p>Limit how many times your ads are shown to the same user</p>
					</header>
					<div class='freq'>
						<Radio name='limit' id='limit-procy'
						 label='Let Deal Procy optimize how often your ads are shown (recommended)' />
						<Radio name='limit' id='limit-set'
						 label='Set a limit' />
					</div>
					<div class='freq'>
						Limit impressions for
						<Dropdown
						 inline
						 options={scale} />
						to
						<input type='number' />
						per
						<Dropdown
						 inline
						 options={period}
						 placeholder='Period' />
					</div>
				</section>
				
				<div id='create-campaign' class='ralign'>
					<Button primary size='big'>Create campaign</Button>
					<Button secondary size='big'>Cancel</Button>
				</div>
			</>
		);
	}
}
