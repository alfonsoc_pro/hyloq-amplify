import React, {Component} from 'react';

export default class DashboardComponent extends Component {
	handleItemClick = (e, {name}) => {
		this.props.shortcut(name);  
	}
}
